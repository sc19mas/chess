import { render, screen, act, fireEvent, cleanup } from '../test-utils'
import Home from '@/pages/index'
import '@testing-library/jest-dom'

const onClick = jest.fn()


describe('Login', () => {
    afterEach(cleanup)
    it('renders all the headings', async () => {
        await act( async () => render(<Home/>));


        const heading = screen.getByRole('heading', {
        name: /Login/i,
        name: /Queen's Chess/i,
        name: /Don't have an account?/i
        })

        expect(heading).toBeInTheDocument()
    })
    it('renders all the buttons', async () => {
        await act( async () => render(<Home />))

        const button = screen.getByRole('button', {
            name: /Login/i,
        })
        button.onclick = onClick
        const p = screen.getByRole('status')
        fireEvent.click(button)
        // screen.debug()
        expect(onClick).toHaveBeenCalledTimes(1);
        expect(screen.queryByTestId('no-error')).toBeInTheDocument()
        expect(button).toBeInTheDocument()
    })
    it('renders all the textareas', async () => {
        await act( async () => render(<Home />))

        const textarea = screen.getByRole('textbox', {
            name: /Username or Email/i
        })

        expect(textarea).toBeInTheDocument()
    })
})
