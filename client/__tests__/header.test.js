import { render, screen, act, fireEvent, cleanup } from '../test-utils'
import Header from '@/components/Dashboard/Header'
import '@testing-library/jest-dom'

describe('Header', () => {
    afterEach(cleanup)
    it('renders all the headings', async () => {
        await act( async () => render(<Header/>));


        const heading = screen.getByRole('heading', {
            name: ''
        })

        expect(heading).toBeInTheDocument()
})})

