import { render, screen, act, fireEvent, cleanup } from '../test-utils'
import News from '@/components/News/News'
import '@testing-library/jest-dom'

describe('News', () => {
    afterEach(cleanup)
    it('renders all the news data', async () => {
        await act( async () => render(<News/>));


        const heading = screen.getByRole('heading', {
            name: /Loading.../i
        })

        const presentation = screen.getByRole('presentation', {
            name : ''
        })

        expect(heading).toBeInTheDocument()
        expect(presentation).toBeInTheDocument()
})})
