import { render, screen, act, fireEvent, cleanup } from '../test-utils'
import Friends from '@/components/Friends/MyFriends'
import SearchForFriends from '@/components/Friends/SearchForOtherPlayers'
import pendingFriendRequests from '@/components/Friends/FriendRequests'
import '@testing-library/jest-dom'

describe('Login', () => {
    afterEach(cleanup)
    it('renders all the headings', async () => {
        await act( async () => render(<Friends />));

        const table = screen.getByRole('table', {
            name : ""
        })


        const row = screen.getByRole('row', {
            name : /Name/i
        })

        const columnHeader = screen.getByRole('columnheader', {
            name: /Name/i,
            name: ""
        })

        expect(table).toBeInTheDocument()
        expect(row).toBeInTheDocument()
        expect(columnHeader).toBeInTheDocument()
})
    it('renders the search friends UI', async () => {
        await act( async () => render(<SearchForFriends />));

        const textbox = screen.getByRole('textbox', {
            name : ""
        })


        const button = screen.getByRole('button', {
            name : /Search/i
        })

        const image = screen.getByRole('img', {
            name: /Search/i,
        })

        expect(textbox).toBeInTheDocument()
        expect(button).toBeInTheDocument()
        expect(image).toBeInTheDocument()
    })
})