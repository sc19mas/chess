import { render, screen, act, fireEvent, cleanup } from '../test-utils'
import Navbar from '@/components/Navbar/Navbar'
import '@testing-library/jest-dom'

describe('Navbar', () => {
    afterEach(cleanup)
    it('renders all the navbar data', async () => {
        await act( async () => render(<Navbar/>));


        const button = screen.getByRole('button', {
            name: /Dashboard/i,
            name : /Friends/i,
            name : /Play/i,
            name : /News/i
        })

        const heading = screen.getByRole('heading', {
            name : /Queen's Chess/i
        })

        expect(button).toBeInTheDocument()
        expect(heading).toBeInTheDocument()
})})
