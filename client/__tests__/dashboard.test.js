import { render, screen, act, fireEvent, cleanup } from '../test-utils'
import Dashboard from '@/components/Dashboard/Content'
import '@testing-library/jest-dom'

describe('Login', () => {
    afterEach(cleanup)
    it('renders all the headings', async () => {
        await act( async () => render(<Dashboard />));


        const heading = screen.getByRole('heading', {
            name: /Best Ways to checkmate your opponent/i,
            name : /A MasterClass/i
        })

        const image = screen.getByRole('img', {
            name : /chess/i
        })

        const link = screen.getByRole('link', {
            name : /Learn More/i
        })

        const button = screen.getByRole('button', {
            name : /Play/i
        })


        expect(button).toBeInTheDocument()
        expect(link).toBeInTheDocument()
        expect(image).toBeInTheDocument()
        expect(heading).toBeInTheDocument()
})})

