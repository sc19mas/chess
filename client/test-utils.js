import { AuthProvider } from "./context/Authentication/AuthContext";
import {render} from '@testing-library/react'
import { Provider } from "react-redux";
import { store } from "./Store/Store";

const AllTheProviders = ({children}) => {
    return(
        <Provider store={store}>
        <AuthProvider>
            {children}
        </AuthProvider>
        </Provider>

    )
}

const customRender = (ui, options) =>
  render(ui, {wrapper: AllTheProviders, ...options})

export * from '@testing-library/react'

export {customRender as render}
