import { createSlice } from "@reduxjs/toolkit"

export const friendSlice = createSlice({
    name: 'friend',
    initialState: { value : {
        serverUpdate : false
    }},
    reducers : {
        friendUpdate : (state, action) => {
            console.log(action.payload);
            state.value = action.payload
        }
    }
})

export const {friendUpdate} = friendSlice.actions

export default friendSlice.reducer
