import { createSlice } from "@reduxjs/toolkit"

export const guestSlice = createSlice({
    name: 'guest',
    initialState: { value : {
        username : ''
    }},
    reducers : {
        userPlay : (state, action) => {
            console.log('State updated!');
            console.log(action.payload);
            state.value = action.payload
        }
    }
})

export const {userPlay} = guestSlice.actions

export default guestSlice.reducer
