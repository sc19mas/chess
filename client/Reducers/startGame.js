import { createSlice } from "@reduxjs/toolkit"

export const playSlice = createSlice({
    name: 'play',
    initialState: { value : {
        doWeStart : false
    }},
    reducers : {
        startPlay : (state, action) => {
            state.value = action.payload
        }
    }
})

export const {startPlay} = playSlice.actions

export default playSlice.reducer