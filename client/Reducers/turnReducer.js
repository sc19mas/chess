import { createSlice } from "@reduxjs/toolkit"

export const turnSlice = createSlice({
    name: 'turn',
    initialState: { value : {
        myTurn : false
    }},
    reducers : {
        setTurn : (state, action) => {
            state.value = action.payload
        }
    }
})

export const {setTurn} = turnSlice.actions

export default turnSlice.reducer