import { createSlice } from "@reduxjs/toolkit"

export const colourSlice = createSlice({
    name: 'colour',
    initialState: { value : {
        colour : 'white'
    }},
    reducers : {
        setColour : (state, action) => {
            state.value = action.payload
        }
    }
})

export const {setColour} = colourSlice.actions

export default colourSlice.reducer