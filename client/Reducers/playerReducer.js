import { createSlice } from "@reduxjs/toolkit"

export const playerSlice = createSlice({
    name: 'player',
    initialState: { value : {
        playerName : ''
    }},
    reducers : {
        playerUpdate : (state, action) => {
            state.value = action.payload
        }
    }
})

export const {playerUpdate} = playerSlice.actions

export default playerSlice.reducer
