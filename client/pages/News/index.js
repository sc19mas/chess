import React from 'react'
import Navbar from '../../components/Navbar/Navbar'
import News from '../../components/News/News'

const index = () => {
  return (
    <div className='flex flex-row w-full'>
    <div className='w-2/12'>
        <Navbar query = 'News'/>
    </div>
        <News />
    </div>
  )
}

export default index