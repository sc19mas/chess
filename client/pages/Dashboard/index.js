import React, { useContext, useEffect } from 'react'
import nookies from 'nookies'
import { admin } from '../../context/Admin/firebase-admin';
import Styles from  '../../styles/Login.module.css'
import Navbar from '../../components/Navbar/Navbar';
import Content from '../../components/Dashboard/Content';
import { useAuth } from '../../context/Authentication/AuthContext';
import { SocketContext } from '../../context/Socket/SocketContext';


export const getServerSideProps = async(ctx) => {
    try{
        const cookies = nookies.get(ctx)
        const token = await admin.auth().verifyIdToken(cookies.token)
        const {uid, email} = token
        return {
            props: { message: `Your email is ${email} and your UID is ${uid}.` },
        };
    }
    catch (err) {
        console.log(err);
        // either the `token` cookie didn't exist
        // or token verification failed
        // either way: redirect to the login page
        // either the `token` cookie didn't exist
        // or token verification failed
        // either way: redirect to the login page
        return {
          redirect: {
            permanent: false,
            destination: "/",
          },
          props: {},
        }
      }
    }



const index = () => {
    const {currentUser} = useAuth()
    
    return (
        <div className={`flex flex-row justify-start w-full`}>
            <div className='w-2/12'>
                <Navbar query = 'Dashboard'/>
            </div>
            <Content />
        </div>
    )
}

export default index