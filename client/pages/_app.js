import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'tailwindcss/tailwind.css'
import { AuthProvider } from '../context/Authentication/AuthContext';
import { Provider } from 'react-redux';
import { store } from '../Store/Store';
import { socket, SocketContext } from '../context/Socket/SocketContext';

function MyApp({ Component, pageProps }) {

  return (
  <SocketContext.Provider value = {socket}>
    <AuthProvider>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </AuthProvider>
  </SocketContext.Provider>
  )
}

export default MyApp
