import { useRouter } from 'next/router'
import React from 'react'
import Navbar from '../../components/Navbar/Navbar'
import { useAuth } from '../../context/Authentication/AuthContext'
import {useSelector} from 'react-redux'
import Content from '../../components/Chess/Content'
import Guest from '../../components/Chess/Guest/Guest'
import ChessFriend from '../../components/Chess/Friend/ChessFriend'


const Play = () => {

  const router = useRouter()
  const {currentUser} = useAuth()
  const gameID = router.query

  if(gameID.SetLink){
    return (
        <div className='flex flex-row w-full'>
            <div className='w-2/12'>
                  <Navbar query = 'Play'/>
              </div>
              <div className='w-full'>
                <Content />
              </div>
        </div>
      )
  }
  else{
    return (
      <div className=''>
        {currentUser?
        <div className='flex flex-row w-full'>
          <div className='w-2/12'>
            <Navbar query = 'Play'/>
          </div>
          <div className='w-full'>
            <ChessFriend />
          </div>
        </div>
        :
        <div className='flex flex-row w-full'>
          <div className='flex flex-row h-screen w-full justify-center items-center' style = {{backgroundColor:'#F6BE00'}}>
            <Guest />
          </div>
        </div>
        }
      </div>
    )
  }
}

export default Play