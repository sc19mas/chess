import Head from 'next/head'
import router from 'next/router'
import { useAuth } from '../context/Authentication/AuthContext'
import { useEffect } from 'react'
import Login from '../components/Login/Login'
import Styles from '../styles/Login.module.css'

  const Home = () => {
    
    // const {currentUser}  = useAuth()

    // useEffect(() => {
    //   if(currentUser)
    //     router.replace('/Dashboard')
    // }, [currentUser])

    return (
      <div className={`flex flex-row h-screen justify-center items-center ${Styles.Background}`}>
        <Head>
          <title>Queen's Chess</title>
          <meta name="description" content="A chess-playing application" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Login />
      </div>
      
  )
}

export default Home