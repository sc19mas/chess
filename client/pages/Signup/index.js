import React from 'react'
import Signup from '../../components/Signup/Signup'
import Styles from  '../../styles/Login.module.css'

const index = () => {
  return (
    <div className={`flex flex-row h-screen justify-center items-center self-center ${Styles.Background}`}>
        <div className='h-5/6 w-full mx-auto'>
        <Signup />
        </div>
    </div>
  )
}

export default index