import React from 'react'
import Friends from '../../components/Friends/Friends'
import Navbar from '../../components/Navbar/Navbar'

const index = () => {
  return (
    <div className='flex flex-row w-full'>
        <div className='w-2/12'>
          <Navbar query = 'Friends'/>
        </div>
        <Friends />
    </div>
  )
}

export default index