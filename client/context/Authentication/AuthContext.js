import React, { useContext, useEffect, useState } from "react";
import { auth } from "../firebase";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  updateProfile,
  signInWithEmailAndPassword,
  signOut,
  onIdTokenChanged,
} from "@firebase/auth";
import {
  getFirestore,
  collection,
  addDoc,
  query,
  where,
  getDocs,
  doc,
  getDoc,
  updateDoc,
  arrayUnion,
  arrayRemove,
} from "firebase/firestore";
const bcrypt = require("bcryptjs");
import nookies from "nookies";

const AuthContext = React.createContext();
const db = getFirestore();

export function useAuth() {
  return useContext(AuthContext);
}

export const createUserDocument = async (user, additionalData) => {
  //Called when sign up is called
  //Creates a new user document in firestore
  if (!user) return;

  const { email } = user;
  const { nickName, password } = additionalData;
  const passwordHash = bcrypt.hashSync(password, 10);

  try {
    //User fields
    await addDoc(collection(db, "users"), {
      email: email,
      id: auth.currentUser.uid,
      password: passwordHash,
      username: nickName,
      friends : [],
      sentFriendRequests : [],
      pendingFriendRequests : [],
      challenge : {
        value : false,
        from : ''
      }
    })

    await addDoc(collection(db, "usernames"), {
      username: nickName,
    })
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}




export const addFriendsToPendingList = async ({ friend }) => {
  //Called when a friend request is sent to another player
  if (!friend) return;
  try {
    const userRef = collection(db, "users");
    const friendRef = query(userRef, where('username', '==', friend))
    const currentUserRef = query(userRef, where('username', '==', auth.currentUser.displayName))

    //Query document of person friend request has been sent to
    const friendQ = getDocs(friendRef)
    //Query user document
    const userQ = getDocs(currentUserRef)

    let friendID
    let friendDetails
    ;(await friendQ).forEach((doc) => {
        friendID = doc.id
        friendDetails = doc.data().username
    })
    let userID
    let userDetails
    ;(await userQ).forEach((doc) => {
        userID = doc.id
        userDetails = doc.data().username
    })
    const userDoc = doc(db, 'users', userID)
    const friendDoc = doc(db, 'users', friendID)
    //Update sentFriendRequests Array of user Doc(Friend request sent)
    await updateDoc(userDoc,{
        sentFriendRequests : arrayUnion({
            id : friendID,
            username : friendDetails
        })
    })

    //Update pendingFriendRequests Array of friend Doc(Friend request received)
    await updateDoc(friendDoc, {
        pendingFriendRequests : arrayUnion({
            id : userID,
            username: userDetails
        })
    })
  }

  catch(e){
      console.log('Error Adding Document : ' , e);
  }
    
};

export const addFriend = async (username) => {
  if(!username) return;
  const userRef = collection(db, 'users')
  const currentUserRef = query(userRef, where('username', '==', auth.currentUser.displayName))
  const friendRef = query(userRef, where('username','==', username))
  const friendQ = getDocs(friendRef)
  const userQ = getDocs(currentUserRef)
  let userID
  let userDetails
  let friendID
  ;(await friendQ).forEach((doc) => {
    friendID = doc.id
  })

  ;(await userQ).forEach((doc) => {
    userID = doc.id
    userDetails = doc.data().username
  })

  const userDoc = doc(db, 'users', userID)
  const friendDoc = doc(db, 'users', friendID)
  //Add friend ID and details(name) to user document
  await updateDoc(friendDoc,{
    friends : arrayUnion(userDetails),
    sentFriendRequests : arrayRemove({
      id: userID,
      username : userDetails
    })
  })

  //Add user ID and details(name) to friend document
  await updateDoc(userDoc,{
    friends : arrayUnion(username),
    pendingFriendRequests : arrayRemove({
      id : friendID,
      username:username
    })
  })
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);

  async function signup(email, password) {
    return createUserWithEmailAndPassword(auth, email, password);
  }

  async function updateUserProfile(username) {
    //Add additionl entries to profile
    updateProfile(auth.currentUser, {
      displayName: username,
    });
  }

  async function login(username, password) {
    //Log user in
    const userRef = collection(db, "users");
    const q = query(userRef, where("username", "==", username));
    const querySnapshot = await getDocs(q);
    let userPassword;
    let userEmail;
    querySnapshot.forEach((f) => {
      userPassword = f.data().password;
      userEmail = f.data().email;
    });
    if (querySnapshot.size === 1 && bcrypt.compareSync(password, userPassword))
      return signInWithEmailAndPassword(auth, userEmail, password);
    //validation failed
    else return false;
  }

  async function checkUsernameAvailability(incompleteUsername) {
    //Check if username is available in the database
    const usernameRef = collection(db, "usernames");
    const q = query(usernameRef, where("username", "==", incompleteUsername));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size === 0) return true;
    return false;
  }

  async function getUserFriendDetails() {
    //Get a list of all friends of the current user
    const userRef = collection(db,"users");
    const q  = query(userRef, where('username' , '==', currentUser.displayName))
    const userSnapshot = getDocs(q)
    let userDetails
    ;(await userSnapshot).forEach((doc) => {
      userDetails = doc.data()
    })
    return userDetails
  }

  async function findPlayers(playerName) {
    //Find other players who have an account
    //Based on query string entered by user
    const userRef = collection(db, "users");
    const q = query(
      userRef,
      where("username", ">=", playerName),
      where("username", "!=", currentUser.displayName),
      where("username", "<=", playerName)
    );
    const querySnapshot = await getDocs(q);
    let allPlayers = [];
    querySnapshot.forEach((doc) => {
      allPlayers.push(doc.data().username);
    });
    return allPlayers;
  }

  async function logout() {
    //Logout
    signOut(auth);
  }
  

  useEffect(() => {
    //Handle state changes
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setCurrentUser(user);
      setLoading(false);
    });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    //Create session tokens for current user
    if (typeof window !== "undefined") {
      window.nookies = nookies;
    }
    return onIdTokenChanged(auth, async (user) => {
      if (!user) {
        setCurrentUser(null);
        nookies.destroy(null, "token");
        nookies.set(null, "token", "", { path: "/" });
        return;
      }

      const token = await user.getIdToken();
      setCurrentUser(user);
      nookies.destroy(null, "token");
      nookies.set(null, "token", token, { path: "/" });
    });
  }, []);

  useEffect(() => {
    //Refresh tokens after 1000 hours
    const handle = setInterval(async () => {
      const user = auth.currentUser;
      if (user) await user.getIdToken(true);
    }, 60 * 60 * 1000);
    return () => clearInterval(handle);
  }, []);

  const value = {
    currentUser,
    signup,
    login,
    updateUserProfile,
    checkUsernameAvailability,
    logout,
    findPlayers,
    getUserFriendDetails,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
