import admin from 'firebase-admin'
import { fireConfig } from '../config/fireConfig'

if(!admin.apps.length){
    try{
        admin.initializeApp({
            credential:admin.credential.cert({
                projectId :process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
                clientEmail : process.env.NEXT_PUBLIC_FIREBASE_CLIENT_EMAIL,
                privateKey : process.env.NEXT_PUBLIC_FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n')
            }),
            databaseURL:process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL
        },
        ),

        console.log('Initialized app!')
    }
    catch(e){
        console.error('Firebase admin initialization error', e.stack)
    }
}

export { admin }