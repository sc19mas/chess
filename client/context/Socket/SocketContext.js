import io from "socket.io-client";
import React from "react";
export const socket = process.env.NODE_ENV === 'production'?  io('https://queen-chess.herokuapp.com/') : io('http://localhost:9000/') 


//Set up initial socket configuration
let mySocketId

socket.on("createNewGame", statusUpdate => {
    console.log("A new game has been created! Username: " + statusUpdate.userName + ", Game id: " + statusUpdate.gameId + " Socket id: " + statusUpdate.mySocketId)
    mySocketId = statusUpdate.mySocketId
})

export {mySocketId}

export const SocketContext = React.createContext();
