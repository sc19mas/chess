import {configureStore} from '@reduxjs/toolkit'
import  colourSlice  from '../Reducers/colourReducer.js'
import  friendSlice  from '../Reducers/friendReducer.js'
import  guestSlice  from '../Reducers/guestReducer.js'
import  playerSlice  from '../Reducers/playerReducer.js'
import  playSlice  from '../Reducers/startGame.js'
import  turnSlice  from '../Reducers/turnReducer.js'

export const store = configureStore({
    reducer : {
        guest : guestSlice,
        play : playSlice,
        colour : colourSlice,
        turn : turnSlice,
        friend : friendSlice,
        player : playerSlice
    }
})


