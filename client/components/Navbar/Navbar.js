import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useAuth } from '../../context/Authentication/AuthContext'
import Styles from '../../styles/Navbar.module.css'
import Navlink from './Navlink'
import { v4 as uuidv4 } from 'uuid';

const Navbar = (props) => {


  const {logout} = useAuth()
  
  const router = useRouter()
  const [show, setShow] = useState(false)
  const [hideNav, setHideNav] = useState(false)

  const uuid = uuidv4()
  return (
    <div className={`flex flex-col h-screen ${Styles.Navbar} w-full`}>
        <div className='flex mx-auto py-10'>
            <h6 className='text-yellow-400 font-bold text-xl'>Queen's Chess</h6>
          <button className='absolute md:-right-5 mid-md:-right-8 semi-md:-right-11 lg:-right-12 top-7' onClick={() => setHideNav(!hideNav)}></button>
        </div>
        <div className={``}>
          <div className='flex flex-col py-4'>
            <Navlink  heading='Dashboard' link = 'Dashboard'  selectedBox = {props.query} />
            <Navlink  heading='Friends' link = 'Friends'  selectedBox = {props.query} />
            <Navlink  heading='Play' link= {`Play/${uuid}?SetLink=True`}  selectedBox = {props.query} />
            <Navlink  heading='News' link= 'News'  selectedBox = {props.query} />
          </div>

        </div>
      </div>
  )
}

export default Navbar