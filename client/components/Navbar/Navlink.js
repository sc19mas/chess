import Link from 'next/link'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startPlay } from '../../Reducers/startGame'
import Styles from '../../styles/Navbar.module.css'


const Navlink = (props) => {
    const dispatch = useDispatch()
    return (
        <Link href={`/${props.link}`}>
        <div 
        className={props.selectedBox === props.heading?`flex flex-row items-center  justify-center py-3 cursor-pointer w-full ${Styles.Active}` : 
        `flex flex-row items-center  justify-center py-3 cursor-pointer w-full text-white`} 
        >
            <button onClick={() => dispatch(startPlay({doWeStart: false}))} className='self-center font-normal md:text-sm xl:text-lg'>{props.heading}</button>
        </div>
        </Link>
    )
}

export default Navlink