import React, { useState } from 'react'
import Styles from '../../styles/Login.module.css'
import Button from '../Button'
import Link from 'next/link';
import { TextField } from '@mui/material';
import { createUserDocument, useAuth } from '../../context/Authentication/AuthContext';
import { useRouter } from 'next/router';

const Signup = () => {
    const router = useRouter()
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('') 
    const [available, setAvailable] = useState(true)
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    const {updateUserProfile, login, signup, checkUsernameAvailability} = useAuth()

    const handleSignup  = async function(e){
        e.preventDefault()
        if(password !== confirmPassword)
            return setError('Passwords do not match!')
        if(!available || username === ''){
            
            setUsername('')
            setAvailable(true)
            return setError('This username is taken. Please select a unique username')
        }
        try{
            setLoading(true)
            setError('')
            const {user} = await signup(email, password)
            await updateUserProfile(username)
            await createUserDocument(user, {email : email, password : password, nickName : username})
            await login(username, password)
            router.push('/Dashboard')
        }
        catch(e){
            console.log(e);
            switch(e.code){
                case 'auth/email-already-in-use':
                    setError('This email already exists. Please use another one')
                    break
                case 'auth/weak-password':
                    setError('Your password is too weak. Please make sure it is at least 6 characters long')
                    break
                case 'auth/invalid-email':
                    setError('Please enter a valid email address')
                    break
            }
        }
        setLoading(false)
    }

    const onChangeHandler = function(e){
        setUsername((e.target).value)
        setError('')
        checkUsernameAvailability((e.target).value).then((r) => setAvailable(r))
    }

    return (
        <div className={`flex flex-col items-center mx-auto justify-start bg-white border border-gray-400 rounded-xl self-center md:w-6/12 p-3 lg:w-4/12`}>
            <h6 className={`py-2 ${Styles.Heading} text-4xl font-bold italic`}>Queen's Chess</h6>
            <div className='mt-3'>
                <h6 className={`${Styles.Heading} text-2xl font-semibold text-center`}>Signup</h6>
                {error? <p className='text-center text-red-500 text-xs font-semibold mx-auto mt-2'>{error}</p> : 
                <p className='text-white text-center text-xs font-semibold mx-auto mt-2'>Error:</p>} 

            </div>
            <div className='flex flex-col p-3'>

                <div className='mt-2'>

                <TextField 
                type='text' 
                placeholder = 'John Doe 356' 
                id='username'
                required={true}
                onChange = {(e) => onChangeHandler(e)}
                value = {username}
                label='Username'
                autoComplete='off'
                />
                <div className='my-2'>
                {available? username === ''? <p className='text-white'>Type something</p> : <p className='text-green-400'>{username} is available</p> : <p className='text-danger'>{username} has already been taken.</p>}
                </div>
                </div>
                <div className='mt-2'>
                <TextField 
                type='email' 
                placeholder = 'someone@gmail.com' 
                id='email'
                autoComplete='off'
                required={true}
                onChange = {(e) => setEmail(e.target.value)}
                value = {email}
                label='Email'
                />
                </div>
                <div className='mt-2'>
                <TextField
                type='password' 
                placeholder = '**************' 
                id='email'
                required={true}
                onChange = {(e) => setPassword(e.target.value)}
                value = {password}
                label = 'Password'
                />
                </div>
                <div className='mt-2'>
                <TextField
                type='password' 
                placeholder = '**************' 
                id='email'
                required={true}
                onChange = {(e) => setConfirmPassword(e.target.value)}
                value = {confirmPassword}
                label='Confirm Password'
                />
                </div>

                <Button execFunction={(e) => handleSignup(e)} text = {'Sign up'} style='mt-4 sm:text-xs md:text-lg font-semibold py-2' disabled = {loading} />
                <div className='flex justify-center items-center py-2'>
                    <h6>Already an account? <Link href={'/'} ><span className={`${Styles.Link} text-center cursor-pointer`}>Login.</span></Link></h6>
                </div>
        </div>
        </div>
  )
}

export default Signup