import React, { useContext, useEffect, useState } from 'react'
import { useAuth } from '../../context/Authentication/AuthContext'
import Styles from  '../../styles/Profile.module.css'
import FaceIcon from '@mui/icons-material/Face';
import SettingsIcon from '@mui/icons-material/Settings';
import WidgetsIcon from '@mui/icons-material/Widgets';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { SocketContext } from '../../context/Socket/SocketContext';
import Popup from '../Popup';
import { Box } from '@mui/material';
import Button from '../Button';
import { v4 as uuidv4 } from 'uuid';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { startPlay } from '../../Reducers/startGame';
import { setColour } from '../../Reducers/colourReducer';
import { setTurn } from '../../Reducers/turnReducer';
import HeaderDropdown from '../HeaderDropdown';

const Header = (props) => {
    const {currentUser, logout} = useAuth()
    const socket = useContext(SocketContext)
    const [challenge, setChallenge] = useState(false)
    const [show, setShow] = useState(null)
    const router = useRouter()
    const dispatch = useDispatch()

    useEffect(() => {
        socket?.emit('joinChallengeRoom', currentUser.displayName)
        socket?.on('showChallenge', () => {
            setChallenge(true)
        })
    }, [currentUser?.displayName, socket])

    const acceptChallenge = () => {
        const colours = ['white', 'black']
        const gameID = uuidv4()
        socket.emit('createNewGame', {gameID})
        const randomColour = colours[Math.floor(Math.random() * colours.length)]
        dispatch(setColour({colour : randomColour}))
        randomColour === 'white'?
        dispatch(setTurn({myTurn : true})):
        dispatch(setTurn({myTurn : false}))
        dispatch(startPlay({doWeStart : true}))
        const username = currentUser.displayName
        socket.emit('challengeAccepted', {gameID, username, randomColour})
        router.push({pathname : `/Play/${gameID}`})
    }

    const clickHandler = (e) => {
        setShow(show? null : e.currentTarget)
    }

    const logoutHandler = () => {
        logout()
        router.push('/')
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,
        bgcolor: '#fff',
        border: '0.5px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: 16
      };
    const open = Boolean(show)
            
    return (
        <div className='flex flex-row flex-1 justify-end items-center h-20 shadow-xl w-full py-2'>
            <div className='flex px-2'>
                <FaceIcon className='w-10 h-10' />
                <div className='flex items-center px-2'>
                    <h6 className={Styles.Name}>{currentUser && currentUser.displayName}</h6>
                    <ArrowDropDownIcon onClick={clickHandler} className='cursor-pointer' />
                    
                <HeaderDropdown open={open} show={show} clickHandler = {logoutHandler} />
                </div>

            </div>
            {
                challenge && 
                <Popup open={challenge}>
                    <Box sx = {style}>
                        <div className='flex flex-col justify-center items-center w-full'>
                            <h6 className={`font-semibold text-2xl mt-3`}>Challenge</h6>
                            <h6 className={`font-normal text-lg mt-3 text-center`}>You have received a challenge!</h6>
                            <div className='flex justify-around pt-5 pb-3'>
                            <button className={` border-2 border-black py-2 px-4 font-semibold text-lg w-52 rounded-3xl` } onClick={() => setChallenge(false)}>Cancel</button>
                            <Button execFunction={acceptChallenge} text='Accept' style='w-52 text-lg font-semibold px-4 py-2 ml-2'  />
                            </div>
                        </div>
                    </Box>
                </Popup>
            }
        </div>
    )
}

export default Header