import router from 'next/router'
import React, { useContext, useState } from 'react'
import Button from '../Button'
import IFrame from '../IFrame'
import Header from './Header'
import { v4 as uuidv4 } from 'uuid';
import {SocketContext} from '../../context/Socket/SocketContext'
import { useDispatch } from 'react-redux'
import { startPlay } from '../../Reducers/startGame'

const Content = () => {
    const [gameCode, setGameID] = useState('')
    const socket = useContext(SocketContext)
    const dispatch = useDispatch()

    const clickHandler = () => {

        const gameID = uuidv4()
        setGameID(gameID)
        socket.emit('createNewGame', {gameID})
        dispatch(startPlay({doWeStart : false}))
        router.push({pathname : `/Play/${gameID}`, query : {SetLink : true}})
    }

    return (
        <div className='flex flex-col justify-start items-start h-full w-full'>
        <Header />
        <div className='flex flex-row justify-around w-full py-3'>
            <div className='flex flex-col self-start flex-nowrap'>
                    <div>
                        
                    </div>
                    <img src={'./chess.jpg'} alt='chess' className='h-80 w-full' />
                    <div className='mt-3'>
                        <h3 className='font-bold text-2xl'>
                            Best ways to checkmate your opponent
                        </h3>
                        <h6 className='font-light'>A MasterClass</h6>
                        <div>
                            <a href='https://www.masterclass.com/articles/chess-checkmate-explained' target={'_blank'} className='font-bold text-blue-600 text-decoration-none'>Learn More</a>
                        </div>
                    </div>
            </div>
        </div>
        <div className='mx-auto py-4'>
            <Button text = 'Play' style = 'px-20 py-2' execFunction = {() => clickHandler()} />
        </div>
        </div>
    )
}

export default Content