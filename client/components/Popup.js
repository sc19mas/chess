import { Modal } from '@mui/material'
import React from 'react'

const Popup = ({open, children}) => {
  return (
    <Modal
    open={open}
    onClose={() => console.log('')}
    aria-labelledby="parent-modal-title"
    aria-describedby="parent-modal-description"
    >
        {children}
    </Modal>
  )
}

export default Popup