import React from 'react'

const IFrame = (props) => {
  return (
    <iframe src={props.src} allowFullScreen width={500} height={400} />
  )
}

export default IFrame


