import { Card, CardContent, CardHeader, CardMedia, Typography } from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Loading from '../Loading'

const Content = () => {
    const [newsData, setNewsData] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        axios.get('https://newsapi.org/v2/everything?domains=chess.com&apiKey=deaeee7957d3456eaaa1d96a22c8e119', 
        {
            params : {
                _limit : 10
            }
        })
        .then(result => {
            setLoading(false)
            setNewsData(result.data)})
        .catch(err => {
            setLoading(false)
            console.log(err)
        })
    })


  return (
    <div className='flex flex-wrap items-start justify-center w-full mt-10'>
                    {!loading && newsData.articles && newsData.articles.map((news, index) => 
                    <div key={index} className='my-2 px-1'>
                    <a href={news.url} target='_blank'  key={index}>
                    <Card className='h-96 w-96 px-4' >
                        <CardMedia variant='top' image={news.urlToImage} alt='chess image' className='h-72' />
                        <CardContent>
                            <Typography variant='p' className='text-center'>{news.title}</Typography>
                        </CardContent>
                    </Card>
                    </a>
                    </div>
                    )}
                    {
                        loading? <Loading loading={loading} /> : <></>
                    }
    </div>

  )
}

export default Content