import React from 'react'
import Header from '../Dashboard/Header'
import Content from './Content'

const News = () => {
  return (
    <div className='flex flex-col justify-start items-start h-full w-full'>
    <Header />
    <div>
        <Content />
    </div>
    </div>
  )
}

export default News