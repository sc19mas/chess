import React from 'react'
import Styles from '../styles/Login.module.css'

 
 const Button = (props) => {
   return (
       <button className={props.active?`${Styles.ActiveButton} ${props.style}` :`${Styles.Button} ${props.style}`} onClick={props.execFunction} disabled = {props.disabled}>{props.text}</button>
   )
 }
 
 export default Button