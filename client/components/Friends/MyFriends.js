import React, { useContext, useEffect, useState } from 'react'
import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import Button from '../Button'
import Searchbar from '../Searchbar'
import { SocketContext } from '../../context/Socket/SocketContext'
import { useRouter } from 'next/router'
import { setColour } from '../../Reducers/colourReducer'
import { useDispatch } from 'react-redux'
import { setTurn } from '../../Reducers/turnReducer'
import { startPlay } from '../../Reducers/startGame'

const MyFriends = ({userFriends, onChangeHandler}) => {
    const socket = useContext(SocketContext)
    const router = useRouter()
    const dispatch = useDispatch()
    useEffect(() =>{
        let isMounted = true
        if(isMounted){

        
        socket?.on('colour', colour => {
            if(isMounted){
            dispatch(setColour({colour : colour}))
            colour === 'white'?
            dispatch(setTurn({myTurn : true})):
            dispatch(setTurn({myTurn : false}))
            dispatch(startPlay({doWeStart : true}))
            }
        })
        socket?.on('gameLink', link => {
            if(isMounted)
                router.push(`/Play/${link}`)
        })
        }
    }, [socket])
    
    return (
        <div>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {
                    userFriends?.friends && userFriends?.friends.map((user, index) => {
                        return(
                            <TableRow key={index}>
                                <TableCell>{user}</TableCell>
                                <TableCell><Button text = 'Challenge' style = 'py-2 px-4' execFunction = {() => socket.emit('challenge', user)}  /></TableCell>
                            </TableRow>
                        )
                    })
                }
                </TableBody>
            </Table>
        </div>
    )
}

export default MyFriends