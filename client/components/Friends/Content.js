import React, { useEffect, useState } from 'react'
import { useAuth } from '../../context/Authentication/AuthContext'
import Button from '../Button'
import MyFriends from './MyFriends'
import FriendRequests from './FriendRequests'
import SearchForOtherPlayers from './SearchForOtherPlayers'
import { addFriendsToPendingList, addFriend } from '../../context/Authentication/AuthContext'
import { useSelector } from 'react-redux'
import Loading from '../Loading'

const Content = () => {

    const friends = useSelector((state) => state.friend.value)
    const {currentUser, findFriends, findPlayers, getUserFriendDetails} = useAuth()
    const [players, setPlayers] = useState([])
    const [selected, setSelected] = useState('friends')
    const [userFriends, setUserFriends] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(() =>{
        async function getUser(){
            const userFriendDetails = await getUserFriendDetails()
            setUserFriends(userFriendDetails)
            setLoading(false)
        }
        if(friends.serverUpdate){
            getUser()
        }
        getUser()
    }, [friends])

    const clickHandler = (value) => {
        setSelected(value)
    }

    const onChangeHandler = async (e) => {
        const playersInGame = await findPlayers(e.target.value)
        setPlayers(playersInGame)
    }

    return (
        <div className='flex flex-col p-8'>
            <div className='flex flex-row py-5'>
                <Button disabled = {false} text = 'My Friends' style = 'px-4 py-2' execFunction = {() => clickHandler('friends')} active={selected === 'friends'? true : false} />
                <Button disabled = {false} text = 'Friend Requests' style = 'px-4 py-2 ml-5' execFunction = {() => clickHandler('friendRequests')} active={selected === 'friendRequests'? true : false} />
                <Button disabled = {false} text = 'Search for other players' style = 'px-4 py-2 ml-5' execFunction = {() => clickHandler('search')} active={selected !== 'friends' && selected !== 'friendRequests'? true : false} />
            </div>
            {selected === 'friends'? 
            <MyFriends userFriends={userFriends} onChangeHandler = {onChangeHandler} /> 
            : 
            selected === 'friendRequests'? 
            <FriendRequests pendingFriendRequests = {userFriends} addFriends = {addFriend} /> 
            : 
            <SearchForOtherPlayers userFriends={userFriends} addFriendsToPendingList= {addFriendsToPendingList} />}
            {loading? <Loading loading = {loading} /> : <></>}
        </div>
    )
}

export default Content