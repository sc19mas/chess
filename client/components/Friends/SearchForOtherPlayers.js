import { Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useAuth } from '../../context/Authentication/AuthContext'
import Styles from '../../styles/Navbar.module.css'
import Button from '../Button'
import Searchbar from '../Searchbar'
import { useDispatch } from 'react-redux'
import { friendUpdate } from '../../Reducers/friendReducer'

const SearchForOtherPlayers = ({userFriends, addFriendsToPendingList}) => {
    const {findPlayers} = useAuth()
    const [players, setPlayers] = useState([])
    const dispatch = useDispatch()

    const onChangeHandler = async (e) => {
        const playersInGame = await findPlayers(e.target.value)
        setPlayers(playersInGame)
        dispatch(friendUpdate({serverUpdate : true}))
    }

    const execFunction = async (friend) => {
        try{
            await addFriendsToPendingList({friend})
            
        }
        catch(e){
            console.log(e);
        }
    }

    return (
        <div>
            <Searchbar onChangeHandler={onChangeHandler} />
            {players.length > 0?
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {players.map((p, index) => {
                    return(
                        <TableRow key={index}>
                            <TableCell>
                                {p}
                            </TableCell>
                            <TableCell>
                                <Button text={userFriends.pendingFriendRequests.find(friend => friend.username === p)? 'Request Sent' : userFriends.friends.includes(p)? 'Friends' : 'Add Friend'} style='px-4 py-2' execFunction = {() => execFunction(p)}/>
                            </TableCell>
                        </TableRow>
                    )
                })}
                
                </TableBody>
            </Table>
             : 
             null}
        </div>
        
    )   
}

export default SearchForOtherPlayers