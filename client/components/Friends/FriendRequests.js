import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useAuth } from '../../context/Authentication/AuthContext'
import { friendUpdate } from '../../Reducers/friendReducer';
import Button from '../Button';

const FriendRequests = ({pendingFriendRequests, addFriends}) => {
  
  const [pendingFriends, setPendingFriends] = useState(pendingFriendRequests)
  const dispatch = useDispatch()
  
  const clickHandler = async (friendName) => {
    await addFriends(friendName)
    dispatch(friendUpdate({serverUpdate: true}))
  }

  if(pendingFriends?.pendingFriendRequests && pendingFriends.pendingFriendRequests.length > 0){
    return(
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {pendingFriends?.pendingFriendRequests && pendingFriends.pendingFriendRequests.map((p, index) => {
          return(
            <TableRow key = {index}>
              <TableCell>{p.username}</TableCell>
              <TableCell>
                <Button text={'Confirm'} style='px-4 py-2' execFunction = {() => clickHandler(p.username)} />
              </TableCell>
            </TableRow>
          )
        })}
        </TableBody>
      </Table>
    )
  }
  else{
    return(
    <Typography variant='h4'>
      You have no pending friend requests!
    </Typography>
    )
  }
}

export default FriendRequests