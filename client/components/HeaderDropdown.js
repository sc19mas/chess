import { Box, Popper } from '@mui/material'
import React from 'react'

const HeaderDropdown = ({open, show, clickHandler}) => {
  return (
      <Popper open={open} anchorEl = {show}>
        <Box sx={{ border: 1, p: 1, width:80, height:42, bgcolor:'#fff', marginRight:3, textAlign:'center' }}>
            <button onClick={clickHandler}>Logout</button>
        </Box>
      </Popper>
  )
}

export default HeaderDropdown