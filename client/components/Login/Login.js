import { useRouter } from 'next/router'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { FormControlLabel, TextField } from '@mui/material';
import Styles from '../../styles/Login.module.css'
import Button from '../Button'
import Link from 'next/link'
import { useAuth } from '../../context/Authentication/AuthContext';

const Login = () => {
  const router = useRouter()
  const {login} = useAuth()
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
    
  const handleLogin = async function(e){
    e.preventDefault()
    try{
      setLoading(true)
      setError('')
      const res = await login(email, password)
      if(!res){
          setError('Wrong username/password combination. Please try again!')
          setLoading(false)
          return
      }
      router.push('/Dashboard')
  }
  catch(e){
      console.log(e);
      setError('Login failed. Please check your credentials and try again!')
    }
    setLoading(false)

  }


  return(
    <div className={`flex flex-col items-center justify-start border border-gray-400 bg-white rounded-xl p-5 md:h-5/6 lg:h-auto md:w-6/12 xl:w-4/12`}>
      <h6 className={`py-2 ${Styles.Heading} text-4xl font-bold italic`}>Queen's Chess</h6>

      <div className='py-3'>
        <h6 className={`${Styles.Heading} text-2xl font-semibold text-center`}>Login</h6>
        {error? <p className='text-center text-red-500 text-xs font-semibold mx-auto' role={'status'} data-testid="error">{error}</p> 
        : <p className='text-center text-white text-xs font-semibold mx-auto' role={'status'} data-testid="no-error">Error:</p>} 
      </div>
      <div className='flex flex-col p-3'>

          <div className='mx-auto'>   
          <TextField
          type='text' 
          placeholder = 'John Doe' 
          id='username'
          required
          onChange = {(e) => {
            setError('')  
            setEmail(e.target.value)}}
          value = {email}
          label='Username or Email'
          />
          </div>
          <div className='mt-5 mx-auto'>
            <TextField
            type='password'
            placeholder = '**************' 
            id='password'
            onChange = {(e) => {
                setError('')
                setPassword(e.target.value)}}
            value = {password}
            required
            label='Password'
            />
          </div>
          <div className='flex justify-center w-full'>
          <Button execFunction={(e) => handleLogin(e)} text = {'Login'} style='mt-5 mx-auto sm:text-xs md:text-lg font-semibold py-2 px-20' disabled={loading} />
          </div>
         <div className='flex flex-col md:mt-2 lg:mt-5'>
          <div className='flex justify-center items-center'>
            <h6>Don't have an account? <Link href={'/Signup'} ><span className={`${Styles.Link} text-center cursor-pointer`}>Sign Up.</span></Link></h6>
          </div>
      </div>
      </div>
    </div>
    )
  }

export default Login