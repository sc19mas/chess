import { Box } from '@mui/system'
import React from 'react'
import Popup from './Popup'
import Styles from '../styles/Loading.module.css'
import { Typography } from '@mui/material'


const Loading = ({loading}) => {
  return (
      <Popup open = {loading}>
          <Box sx = {{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width : {
                            md : 300,
                            lg : 400
                        },
                        height :{
                            md : 250,
                            lg : 300
                        },
                        bgcolor: '#fff',
                        border: '0.5px solid #000',
                        boxShadow: 24,
                        p: 4,
                        borderRadius: '1rem'
            }}>
                <div className='flex flex-col justify-start items-center'>
                    <Typography variant='h6'>Loading...</Typography>
                    <Typography variant='p'>It will only take a few moments</Typography>
                    <div className={`${Styles.Loader}  md:mt-4 lg:mt-5`}>

                    </div>
               </div>
          </Box>
      </Popup>
  )
}

export default Loading