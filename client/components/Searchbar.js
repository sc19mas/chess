import React from 'react'
import Styles from '../styles/Navbar.module.css'
import Image from 'next/image'
const Searchbar = ({onChangeHandler}) => {
  return (
    <div className='relative my-5'>
        <input className={`${Styles.RoleInput} md:w-32 lg:w-40 py-2 pl-8`} placeholder = 'Search...' onChange={onChangeHandler} />
        <button className='absolute left-1 md:top-5 lg:top-2'><Image src = '/search.svg' alt='search' width={25} height={25} /></button>
    </div>
  )
}

export default Searchbar