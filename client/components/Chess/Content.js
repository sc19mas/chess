import { Button, Input, Typography } from '@mui/material'
import router from 'next/router'
import React, { useContext, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SocketContext } from '../../context/Socket/SocketContext'
import { colours } from '../../helpers/constants'
import { setColour } from '../../Reducers/colourReducer'
import { startPlay } from '../../Reducers/startGame'
import { setTurn } from '../../Reducers/turnReducer'
import Header from  '../Dashboard/Header'
import ChessGame from './ChessGame'

const Content = () => {
    const domainName = process.env.NODE_ENV === 'production'? 'https://chess-rfawesome21.vercel.app/Play' : 'http://localhost:3000/Play'
    const socket = useContext(SocketContext)
    const gameID = router.query.id
    const dispatch = useDispatch()
    const playStart = useSelector((state) => state.play.value)

    useEffect(() => {
        socket.emit('joinLinkRoom', {gameID})
    }, [socket])

    useEffect(() => {
        socket.on('playerJoined',() => {
            const randomColour = colours[Math.floor(Math.random() * colours.length)]
            dispatch(setColour({colour : randomColour}))
            socket.emit('selectColour', {gameID, randomColour})
            randomColour === 'white'?
            dispatch(setTurn({myTurn : true})):
            dispatch(setTurn({myTurn : false}))
            dispatch(startPlay({doWeStart : true}))
        })
    }, [socket])

    return (
        <div className='flex flex-col w-full'>
            <Header />
            <div className='flex flex-col w-full justify-start items-center'>
                {!playStart.doWeStart?
                <div>
                <Typography variant = 'h3' className='py-5'>
                    Play with your Friends!
                </Typography>
                <Typography variant = 'h6' className='text-center'>
                    Share this link to get started: 
                    <span>
                        <Button onClick={() => navigator.clipboard.writeText(`${domainName}/${router.query.id}`)} className='w-full'>
                            <Input disabled value={`${domainName}/${router.query.id}`} className='w-full cursor-pointer' />
                        </Button>
                    </span>
                </Typography>
                </div>
                :
                <ChessGame />
                }
            </div>
        </div>
    )
}

export default Content