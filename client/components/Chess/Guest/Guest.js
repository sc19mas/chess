import { TextField } from '@mui/material'
import React, { useContext, useEffect, useState } from 'react'
import Button from '../../Button'
import Styles from '../../../styles/Login.module.css'
import Link from 'next/link'
import {SocketContext} from '../../../context/Socket/SocketContext'
import router from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import ChessGame from '../ChessGame'
import { setColour } from '../../../Reducers/colourReducer'
import { startPlay } from '../../../Reducers/startGame'
import { setTurn } from '../../../Reducers/turnReducer'

const Guest = () => {

    const [playerName, setUsername] = useState('')
    const [error, setError] = useState('')
    const socket = useContext(SocketContext)
    const gameID = router.query.id
    const playStart = useSelector((state) => state.play.value)
    const dispatch = useDispatch()

    useEffect(() => {
        socket.on('err', ({message}) => setError(message))
    }, [socket])

    const execFunction = () => {
        socket.emit('playerJoined', {gameID, playerName})
        socket.on('colour' , colour => {
            colour === 'white'?
            dispatch(setTurn({myTurn : true})):
            dispatch(setTurn({myTurn : false}))
            dispatch(setColour({colour : colour}))
            dispatch(startPlay({doWeStart : true}))
        })
    }

    if(!playStart.doWeStart){
    return (
        <div className='flex flex-col bg-white border-1 border-black px-10'>
            <h6 className={`py-5 ${Styles.Heading} text-4xl font-bold italic`}>Queen's Chess</h6>
            <div className='py-5'>
                {error? <p className='text-center text-red-500 text-xs font-semibold mx-auto'>{error}</p> 
                : 
                <p className='text-center text-white text-xs font-semibold mx-auto'>Error:</p>} 
                <TextField label='Username' onChange={e => setUsername(e.target.value)} placeholder='John Doe' className='' />
            </div>
            <Button text = 'Play' execFunction = {execFunction} style = {'px-20 py-2'} />
            <div className='flex justify-center items-center py-5'>
                <h6>Like what you see? <Link href={'/Signup'} ><span className={`${Styles.Link} text-center cursor-pointer`}>Sign Up.</span></Link></h6>
          </div>
        </div>
    )
    }
    else{
        return(
            <div className='flex flex-col bg-white justify-start items-center'>
                <h6 className={`py-2 ${Styles.Heading} text-4xl font-bold italic`}>Queen's Chess</h6>
                <ChessGame />
            </div>
        )
    }
}

export default Guest