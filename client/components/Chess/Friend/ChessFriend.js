import { useRouter } from 'next/router'
import React, { useContext, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { SocketContext } from '../../../context/Socket/SocketContext'
import { setColour } from '../../../Reducers/colourReducer'
import { startPlay } from '../../../Reducers/startGame'
import { setTurn } from '../../../Reducers/turnReducer'
import Header from '../../Dashboard/Header'
import ChessGame from '../ChessGame'

const ChessFriend = () => {
    const socket = useContext(SocketContext)
    const router = useRouter()
    const gameID = router.query.id
    const dispatch = useDispatch()
    useEffect(() => {
        socket.emit('playerJoined', {gameID})
        socket.on('colour' , colour => {
            colour === 'white'?
            dispatch(setTurn({myTurn : true})):
            dispatch(setTurn({myTurn : false}))
            dispatch(setColour({colour : colour}))
            dispatch(startPlay({doWeStart : true}))
        })
    }, [socket])
  return (
      <div className='flex flex-col w-full'>
          <Header />
      <div className='flex flex-row w-full justify-center'>
      <ChessGame />
      </div>
      </div>
  )
}

export default ChessFriend