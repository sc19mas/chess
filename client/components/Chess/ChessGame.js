import React, { useContext, useEffect, useState } from 'react'
import { Chessboard }from  'react-chessboard'
import Chess from 'chess.js'
import { useDispatch, useSelector } from 'react-redux'
import { setTurn } from '../../Reducers/turnReducer'
import { SocketContext } from '../../context/Socket/SocketContext'
import router from 'next/router'
import { Box, Typography } from '@mui/material'
import { useAuth } from '../../context/Authentication/AuthContext'
import Popup from '../Popup'
import Button from '../Button'
import { colours } from '../../helpers/constants'
import { startPlay } from '../../Reducers/startGame'
import {setColour} from '../../Reducers/colourReducer'

const ChessGame = () => {
    const [game, setGame] = useState(new Chess())
    const [message, setMessage] = useState(false)
    const [stalemate, setStalemate] = useState(false)
    const [winner, setWinner] = useState('')
    const [gameState, setGameState] = useState('')
    const [inCheck, setInCheck] = useState(false)
    const [gameOver, setGameOver] = useState(false)
    const colour = useSelector((state) => state.colour.value)
    const myTurn = useSelector((state) => state.turn.value)
    const socket = useContext(SocketContext)
    const gameID = router.query.id
    const dispatch = useDispatch()

    const playAgain = () => {
        const randomColour = colours[Math.floor(Math.random() * colours.length)]
        socket.emit('selectColour', {gameID, randomColour})
        dispatch(setColour({colour : randomColour}))
        randomColour === 'white'?
        dispatch(setTurn({myTurn : true})):
        dispatch(setTurn({myTurn : false}))
        dispatch(startPlay({doWeStart : true}))
        game.reset()
        setGameOver(false)
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,
        bgcolor: '#fff',
        border: '0.5px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: 16
      };
    
    async function onDrop(sourcePos, targetPos) {
        const gameCopy = { ...game };
        const move = gameCopy.move({
          from: sourcePos,
          to: targetPos,
          promotion: 'q' // always promote to a queen for example simplicity
        });
        setGame(gameCopy);
        if(game.game_over()){
            setGameOver(true)
            setMessage(true)
            setInCheck(false)
            game.history({ verbose : true }).slice(-1)[0].color === 'w'? setWinner('White') : setWinner('Black')

            if(game.in_draw()){
                setStalemate(true)
                setMessage(false)
                setGameState('Draw!')
                setWinner('')
                return
            }
            dispatch(setTurn({myTurn : false}))
        }
        if(game.in_check()){
            game.history({ verbose : true }).slice(-1)[0].color === 'w'? setGameState('Black is in check!') : setGameState('White is in check!')
            setInCheck(true)
        }
        else{
            setInCheck(false)
        }
        
        if(move && myTurn.myTurn){
            dispatch(setTurn({myTurn : false}))
            socket.emit('newMove', {gameID, sourcePos, targetPos})
            return move;
        }
        return move;
    }
    console.log(myTurn.myTurn)

    useEffect(() => {
        let isMounted = true
        if(isMounted){
            socket.on('opponentMove' , ({sourcePos, targetPos}) => {
                if(isMounted && !game.game_over()){
                    onDrop(sourcePos, targetPos)
                    dispatch(setTurn({myTurn : true}))
                }
            })
            if(isMounted){
                socket.on('colour', colour => {
                    dispatch(setColour({colour : colour}))
                    colour === 'white'?
                    dispatch(setTurn({myTurn : true})):
                    dispatch(setTurn({myTurn : false}))
                    dispatch(startPlay({doWeStart : true}))
                    setGameOver(false)
                    game.reset()
                })
            }
        }
        return () => isMounted = false
    }, [socket])

    const isDraggablePiece = (piece) => {
        if(colour.colour === 'white'){
            if(piece.piece.startsWith('b')){
                return false
            }
            return true
        }
        else{
            if(piece.piece.startsWith('w')){
                return false
            }
            return true
        }
    }

    return (
        <div className='py-3 flex flex-col'>
            {message? 
            <Typography variant = 'h6' className='py-1 text-center'>
                {winner} wins!
            </Typography>
            : <></>
            }
            {
                inCheck?
            <Typography variant = 'h6' className='py-1 text-center'>
                {gameState}
            </Typography>  
            : <></>
            }
            {
                stalemate?
            <Typography variant = 'h6' className='py-1 text-center'>
                {gameState}
            </Typography>   
            :
            <></>
            }
            <div>
                <Chessboard position={game.fen()} onPieceDrop = {onDrop} 
                arePiecesDraggable={myTurn.myTurn} boardOrientation={colour.colour} isDraggablePiece = {isDraggablePiece} />
            </div>
            <div className='flex justify-center items-center xl:text-2xl'>
            {
                myTurn.myTurn === true?
                'Your Turn' : ''
            }
            </div>
            {gameOver && 
            <Popup open={gameOver}>
                <Box sx = {style}>
                    <h6 className={`font-semibold text-2xl mt-3 text-center`}>Play Again?</h6>
                    <div className='flex justify-around pt-5 pb-3'>
                        <button className={` border-2 border-black py-2 px-4 font-semibold text-lg w-52 rounded-3xl` } onClick={() => setGameOver(false)}>Cancel</button>
                        <Button execFunction={playAgain} text='Yes' style='w-52 text-lg font-semibold px-4 py-2 ml-2'  />
                    </div>
                </Box>
            </Popup>}
        </div>
    )
}

export default ChessGame