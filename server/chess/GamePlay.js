module.exports = (io, socket) => {

    const createNewGame = ({gameID}) => {
        socket.join(gameID)
        console.log('A new game has been created!');
    }

    const playerJoined = async({gameID}) => {
        const sockets = await io.in(gameID).fetchSockets();
        if(sockets === 'undefined'){
            io.to(socket.id).emit('err', {message : 'The game does not exist!'})
            console.log('The game does not exist!');
        }
        if(sockets.length < 2){
            console.log('A player joined the game!');
            socket.join(gameID)
            const socketsInRoom = await io.in(gameID).fetchSockets();
            console.log(`Sockets in Room : ${socketsInRoom.length}`);
            if(socketsInRoom.length === 2){
                console.log('Starting game...');
                io.to(socket.id).emit('')
                io.in(gameID).emit('playerJoined')
            }
        }
        else{
            console.log('Two players already in the game!');
            io.to(socket.id).emit('err', {message : 'Two players are already in the game!'})
        }
    }

    const joinLinkRoom = async({gameID}) => {
        socket.join(gameID)
        const sockets = await io.in(gameID).fetchSockets();
        console.log(sockets.length);
        if(sockets.length === 2){
            console.log('Starting game...');
            io.in(gameID).emit('playerJoined')
        }
    }

    const selectColour = ({gameID, randomColour}) => {
        randomColour === 'white'?
        socket.to(gameID).emit('colour', 'black')
        :
        socket.to(gameID).emit('colour', 'white')
    }

    const newMove = ({gameID, sourcePos, targetPos}) => {
        socket.to(gameID).emit('opponentMove', {sourcePos, targetPos})
    }

    const joinChallengeRoom = (name) => {
        socket.join(name)
    }

    const challenge = (username) => {
        socket.join(username)
        console.log(username);
        socket.to(username).emit('showChallenge')
    }

    const challengeAccepted = ({gameID, username, randomColour}) => {
        randomColour === 'white'?
        socket.to(username).emit('colour', 'black')
        :
        socket.to(username).emit('colour', 'white')
        socket.to(username).emit('gameLink', gameID)
    }

    socket.on('selectColour', selectColour)
    socket.on('joinLinkRoom', joinLinkRoom)
    socket.on('createNewGame', createNewGame)
    socket.on('playerJoined', playerJoined)
    socket.on('newMove', newMove)
    socket.on('joinChallengeRoom', joinChallengeRoom)
    socket.on('challenge', challenge)
    socket.on('challengeAccepted', challengeAccepted)
}