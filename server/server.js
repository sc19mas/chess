//Initialize the app
const express = require('express')
const cors = require('cors')
const colors = require('colors')
const http = require('http')

const app = express()
app.use(express.json())

const corsOptions ={
  origin:'*', 
  credentials:true,            //access-control-allow-credentials:true
  optionSuccessStatus:200,
}

app.use(cors(corsOptions))

const PORT = process.env.PORT || 9000
const MODE = process.env.MODE || 'development'


const httpServer=http.createServer(app)

const io = require('socket.io')(httpServer, {
    cors: {
      origins: ['http://localhost:3000', '*'],
    },
})

const GamePlay = require('./chess/GamePlay')

const onConnection = socket => {
    GamePlay(io, socket)
}

io.on('connection', onConnection)

httpServer.listen(
    PORT,
    console.log(
      `Server running on port ${PORT} in ${MODE.blue + ' mode'.yellow.bold} `.yellow.bold
    )
)
